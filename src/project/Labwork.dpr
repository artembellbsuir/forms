program Labwork;

uses
  Vcl.Forms,
  Main in '..\views\Main.pas' {MainForm},
  Tree in '..\units\Tree.pas',
  MyLib in '..\units\MyLib.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TMainForm, MainForm);
  Application.Run;
end.
