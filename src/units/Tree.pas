unit Tree;

interface

uses
   System.SysUtils;

type
   TNodePointer = ^TNode;

   TNode = record
      Name: string;
      Right: TNodePointer;
      Left: TNodePointer;
      Parent: TNodePointer;
   end;

   TTree = class
   private
      FHead: TNodePointer;

      function IsEmpty: Boolean;
      procedure FindNode(CurrentNode: TNodePointer; Item: string);
   public
      constructor Create;

      function GetHead: TNode;
      function AddNode(Item, ParentItem: string): TNode;


      procedure DeleteChildrenNodes(Item: string);
      procedure DeleteNode(Item: string);
      procedure InsertTree(Item: string);

      { helpers }
      function GetLeftChild(Node: TNode): TNodePointer;
      function HasLeftChild(Node: TNode): Boolean;
      function GetRightChild(Node: TNode): TNodePointer;
      function HasRightChild(Node: TNode): Boolean;

   end;

implementation

var
   FoundedNode: TNodePointer;

   { TTree }

function TTree.AddNode(Item, ParentItem: string): TNode;
var
   Temp, Current, Parent: TNodePointer;
begin
   New(Temp);

   Temp^.Name := Item;
   Temp^.Right := nil;
   Temp^.Left := nil;

   Parent := nil;

   if IsEmpty then
   begin
      Temp^.Parent := nil;
      FHead := Temp;
   end
   else
   begin
      Current := FHead;

      FindNode(FHead, ParentItem);
      Parent := FoundedNode;

      if Parent^.Left = nil then
         Parent^.Left := Temp
      else if Parent^.Right = nil then
         Parent^.Right := Temp
      else
         raise Exception.Create('This parent already has 2 children! (' +
           Parent^.Name + ')');
   end;

   Temp^.Parent := Parent;
   Result := Temp^;
end;

constructor TTree.Create;
begin
   inherited Create;
   FHead := nil;
end;

procedure TTree.DeleteChildrenNodes(Item: string);
begin
   // FoundedNode := FHead;
   FindNode(FHead, Item);

   // FoundedNode := nil;
   FoundedNode^.Left := nil;
   FoundedNode^.Right := nil;
end;

procedure TTree.DeleteNode(Item: string);
var
   Parent: TNodePointer;

begin
   FindNode(FHead, Item);
   Parent := FoundedNode^.Parent;

   if Parent^.Left = FoundedNode then
      Parent^.Left := nil
   else if Parent^.Right = FoundedNode then
      Parent^.Right := nil

end;

procedure TTree.FindNode(CurrentNode: TNodePointer; Item: string);
var
   Current, Parent, Next: TNodePointer;
   Gone: Boolean;
begin
   if CurrentNode^.Name = Item then
      FoundedNode := CurrentNode;

   if HasLeftChild(CurrentNode^) then
      FindNode(CurrentNode^.Left, Item);

   if HasRightChild(CurrentNode^) then
      FindNode(CurrentNode^.Right, Item);
end;

function TTree.GetHead: TNode;
begin
   Result := FHead^;
end;

function TTree.GetLeftChild(Node: TNode): TNodePointer;
begin
   Result := Node.Left;
end;

function TTree.GetRightChild(Node: TNode): TNodePointer;
begin
   Result := Node.Right;
end;

function TTree.HasLeftChild(Node: TNode): Boolean;
begin
   Result := GetLeftChild(Node) <> nil;
end;

function TTree.HasRightChild(Node: TNode): Boolean;
begin
   Result := GetRightChild(Node) <> nil;
end;

procedure TTree.InsertTree(Item: string);
begin
   { }
end;

function TTree.IsEmpty: Boolean;
begin
   Result := FHead = nil;
end;

end.
