unit MyLib;

interface

uses
   System.SysUtils;

type
   PNode = ^TNode;

   TNode = record
      Name: string[50];
      PParent, PLeft, PRight: PNode;
   end;

function InitTree(var Tree: PNode; NewName: String): PNode;
function AddNode(var ParentNode: PNode; NewName: string): PNode;
procedure FindNode(StartNode: PNode; var FoundedNode: PNode; NodeName: string);
procedure CopyTree(var StartNode, FoundedNode: PNode);
procedure DeleteNode(var WorkingTree, FoundedNode, TreeTop, InsertTree: PNode;
  NodeName: string);
procedure DeleteChildren(var WorkingTree, FoundedNode, TreeTop,
  InsertTree: PNode; NodeName: string);
function NodeExists(var Tree, FoundedNode: PNode; NodeName: string): Boolean;

implementation

function InitTree(var Tree: PNode; NewName: String): PNode;
begin
   New(Tree);

   Tree^.Name := NewName;
   Tree^.PParent := nil;
   Tree^.PLeft := nil;
   Tree^.PRight := nil;

   Result := Tree;
end;

function AddNode(var ParentNode: PNode; NewName: string): PNode;
var
   NewNode: PNode;
begin
   New(NewNode);

   NewNode^.Name := NewName;
   NewNode^.PParent := ParentNode;
   NewNode^.PLeft := nil;
   NewNode^.PRight := nil;

   if ParentNode^.PLeft = nil then
      ParentNode^.PLeft := NewNode
   else if ParentNode^.PRight = nil then
      ParentNode^.PRight := NewNode
   else
      raise Exception.Create('Node can have no more than 2 children!(' +
        ParentNode^.Name + ')');
   Result := NewNode;
end;

procedure FindNode(StartNode: PNode; var FoundedNode: PNode; NodeName: string);
var
   CurNode: PNode;
begin
   if StartNode <> nil then
   begin
      if StartNode^.Name = NodeName then
         FoundedNode := StartNode
      else
      begin
         if StartNode^.PLeft <> nil then
            FindNode(StartNode^.PLeft, FoundedNode, NodeName);

         if StartNode^.PRight <> nil then
            FindNode(StartNode^.PRight, FoundedNode, NodeName);
      end;
   end;

end;

procedure CopyTree(var StartNode, FoundedNode: PNode);
begin
   if FoundedNode <> nil then
   begin
      StartNode^.PLeft := FoundedNode^.PLeft;
      StartNode^.PRight := FoundedNode^.PRight;

      CopyTree(StartNode^.PLeft, FoundedNode^.PLeft);
      CopyTree(StartNode^.PRight, FoundedNode^.PRight);
   end;
end;

procedure DeleteNode(var WorkingTree, FoundedNode, TreeTop, InsertTree: PNode;
  NodeName: string);
begin
   FindNode(WorkingTree, FoundedNode, NodeName);

   if FoundedNode^.PParent = nil then
   begin
      if WorkingTree = TreeTop then
      begin
         Dispose(TreeTop);
         TreeTop := nil;
      end
      else if WorkingTree = InsertTree then
      begin
         Dispose(InsertTree);
         InsertTree := nil;
      end;
   end
   else
   begin
      if (FoundedNode^.PParent^.PLeft <> nil) and
        (FoundedNode^.PParent^.PLeft^.Name = NodeName) then
      begin
         Dispose(FoundedNode^.PParent^.PLeft);
         FoundedNode^.PParent^.PLeft := nil;
      end
      else if (FoundedNode^.PParent^.PRight <> nil) and
        (FoundedNode^.PParent^.PRight^.Name = NodeName) then
      begin
         Dispose(FoundedNode^.PParent^.PRight);
         FoundedNode^.PParent^.PRight := nil;
      end;
   end;
end;

procedure DeleteChildren(var WorkingTree, FoundedNode, TreeTop,
  InsertTree: PNode; NodeName: string);
begin
   FindNode(WorkingTree, FoundedNode, NodeName);

   Dispose(FoundedNode^.PLeft);
   Dispose(FoundedNode^.PRight);
   FoundedNode^.PLeft := nil;
   FoundedNode^.PRight := nil;
end;

function NodeExists(var Tree, FoundedNode: PNode; NodeName: string): Boolean;
begin
   Result := False;
   if Tree <> nil then
   begin
      FindNode(Tree, FoundedNode, NodeName);
      if FoundedNode = nil then
         Result := False
      else
         Result := (FoundedNode^.Name = NodeName);
   end;
end;

end.
