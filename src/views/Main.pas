unit Main;

interface

uses
   Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
   System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
   Vcl.ComCtrls, Vcl.Menus, Vcl.StdCtrls, System.Actions, Vcl.ActnList;
// MyLib;

type
   PNode = ^TNode;

   TNode = record
      Name: string[50];
      PParent, PLeft, PRight: PNode;
   end;

   TMainForm = class(TForm)
      TreeView: TTreeView;
      mnPop: TPopupMenu;
      mnPopAddNode: TMenuItem;
      mnPopDelete: TMenuItem;
      InsertTreeView: TTreeView;
      mnPopDeleteChildren: TMenuItem;
      alMain: TActionList;
      actNodeDeleteChildren: TAction;
      actNodeDelete: TAction;
      actTreeInsertion: TAction;
      actNodeAdd: TAction;
      mnPopInsertTree: TMenuItem;
      btnClearTree: TButton;
      actTreeClear: TAction;
    btnTraverse: TButton;
      actTreeLeftTopRightTraverse: TAction;
      lblHistory: TLabel;
      btnInitTree: TButton;
      actTreeInit: TAction;
      gbMainTree: TGroupBox;
      gbInsertTree: TGroupBox;
      actTreeInitInsert: TAction;
      actTreeClearInsert: TAction;
      btnClearInsertTree: TButton;
      btnInitInsertTree: TButton;
      actExit: TAction;
      mnMain: TMainMenu;
      mnItemHelp: TMenuItem;
      actHelpTask: TAction;
      mnItemTask: TMenuItem;
      actHelpTips: TAction;
      mnItemTips: TMenuItem;
      actTreeUpdateControls: TAction;
      actUpdatePopupControls: TAction;
      procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
      procedure TreeViewContextPopup(Sender: TObject; MousePos: TPoint;
        var Handled: Boolean);

      { actions }
      procedure actNodeAddExecute(Sender: TObject);
      procedure actNodeDeleteExecute(Sender: TObject);
      procedure actNodeDeleteChildrenExecute(Sender: TObject);

      procedure actTreeLeftTopRightTraverseExecute(Sender: TObject);

      procedure actTreeInitExecute(Sender: TObject);
      procedure actTreeInitInsertExecute(Sender: TObject);
      procedure actTreeInsertionExecute(Sender: TObject);
      procedure actTreeClearExecute(Sender: TObject);
      procedure actTreeClearInsertExecute(Sender: TObject);

      procedure actExitExecute(Sender: TObject);
      procedure actHelpTaskExecute(Sender: TObject);
      procedure actTreeUpdateControlsExecute(Sender: TObject);
      procedure actHelpTipsExecute(Sender: TObject);

      { popup }
      procedure mnPopPopup(Sender: TObject);
      procedure actUpdatePopupControlsExecute(Sender: TObject);
      procedure FormCreate(Sender: TObject);

   private
      procedure DrawTrees(Sender: TObject);
      procedure DrawChildren(TreeView: TTreeView; CurrentNode: TTreeNode;
        MyNode: PNode);
      procedure Traverse(CurrentNode: PNode);
      function InputNodeName: string;

   end;

function InitTree(var Tree: PNode; NewName: String): PNode; external 'Lib.dll';
function AddNode(var ParentNode: PNode; NewName: string): PNode;
  external 'Lib.dll';
procedure FindNode(StartNode: PNode; var FoundedNode: PNode; NodeName: string);
  external 'Lib.dll';
procedure CopyTree(var StartNode, FoundedNode: PNode); external 'Lib.dll';
procedure DeleteNode(var WorkingTree, FoundedNode, TreeTop, InsertTree: PNode;
  NodeName: string); external 'Lib.dll';
procedure DeleteChildren(var WorkingTree, FoundedNode, TreeTop,
  InsertTree: PNode; NodeName: string); external 'Lib.dll';
function NodeExists(var Tree, FoundedNode: PNode; NodeName: string): Boolean;
  external 'Lib.dll';

const
   QuitMsg = 'Are you sure you want to quit?';

   EnterNodeNameMsg = 'Enter node name';
   NodeNameMsg = 'Name';
   NameMaxLength = 10;

   TipsMsg = '���������';
   TaskMsg = '����������� ��������� ������ � �������� �������. ��������� ' +
     '��������� ��������� ���������, ���������� �� ����:' + #13#10 +
     ' - ���������� ������� ������' + #13#10 +
     ' - ������� ��������� � ��������� �����' + #13#10 +
     ' - �������� ���������� ��������' + #13#10 +
     ' - �������� ������ � ��������� �������: ����, ����� �����, ������ �����';

var
   MainForm: TMainForm;

   ClickedNode: TTreeNode;

   TreeTop, InsertTree, WorkingTree, FoundedNode: PNode;

implementation

{$R *.dfm}

procedure TMainForm.Traverse(CurrentNode: PNode);
begin
   if CurrentNode <> nil then
   begin
      lblHistory.Caption := lblHistory.Caption + CurrentNode^.Name + ' ';
      Traverse(CurrentNode^.PLeft);
      Traverse(CurrentNode^.PRight);
   end;
end;

{ >>> ACTIONS >>> }

{ >>> ADDING NODE >>> }
procedure TMainForm.actNodeAddExecute(Sender: TObject);
var
   Name: string;
begin

   Name := InputNodeName;

   FindNode(WorkingTree, FoundedNode, ClickedNode.Text);
   AddNode(FoundedNode, Name);

   DrawTrees(Sender);
end;

{ >>> INITIALIZING >>> }

procedure TMainForm.actTreeInitExecute(Sender: TObject);
var
   Node1, Node2, Node3, Node4, Node5, Node6: PNode;
   Name: string;
begin
   Name := InputNodeName;

   InitTree(TreeTop, Name);

   btnClearTree.Enabled := True;
   btnInitTree.Enabled := False;

   WorkingTree := TreeTop;
   DrawTrees(Sender);
end;

procedure TMainForm.actTreeInitInsertExecute(Sender: TObject);
var
   Node1: PNode;
   Name: string;
begin
   Name := InputNodeName;

   InitTree(InsertTree, Name);
   // Node1 := AddNode(InsertTree, 'chiddle');

   btnClearInsertTree.Enabled := True;
   btnInitInsertTree.Enabled := False;

   WorkingTree := InsertTree;
   DrawTrees(Sender);
end;
{ >>> INITIALIZING >>> }

{ >>> CLEARING TREE >>> }
procedure TMainForm.actTreeClearExecute(Sender: TObject);
begin
   Dispose(TreeTop);
   TreeTop := nil;

   btnInitTree.Enabled := True;
   btnClearTree.Enabled := False;
   lblHistory.Caption := '';

   DrawTrees(Sender);
end;

procedure TMainForm.actTreeClearInsertExecute(Sender: TObject);
begin
   Dispose(InsertTree);
   InsertTree := nil;

   btnInitInsertTree.Enabled := True;
   btnClearInsertTree.Enabled := False;

   DrawTrees(Sender);
end;
{ >>> CLEARING TREE >>> }

{ >>> DELETING >>> }
procedure TMainForm.actNodeDeleteChildrenExecute(Sender: TObject);
begin
   DeleteChildren(WorkingTree, FoundedNode, TreeTop, InsertTree,
     ClickedNode.Text);

   DrawTrees(Sender);
end;

procedure TMainForm.actNodeDeleteExecute(Sender: TObject);
begin
   DeleteNode(WorkingTree, FoundedNode, TreeTop, InsertTree, ClickedNode.Text);

   DrawTrees(Sender);
end;
{ >>> DELETING >>> }

procedure TMainForm.actTreeLeftTopRightTraverseExecute(Sender: TObject);
begin
   lblHistory.Caption := '';
   Traverse(TreeTop);
end;

procedure TMainForm.actTreeInsertionExecute(Sender: TObject);
var
   StartNode, MirrorNode: PNode;
begin
   FindNode(TreeTop, FoundedNode, ClickedNode.Text);

   FoundedNode^.PRight := InsertTree;

   StartNode := FoundedNode^.PRight;
   MirrorNode := InsertTree;

   CopyTree(StartNode, MirrorNode);
   InsertTree := nil;

   DrawTrees(Sender);
end;

procedure TMainForm.actTreeUpdateControlsExecute(Sender: TObject);
begin
   btnInitTree.Enabled := TreeView.Items.Count = 0;
   btnClearTree.Enabled := not btnInitTree.Enabled;

   btnInitInsertTree.Enabled := InsertTreeView.Items.Count = 0;
   btnClearInsertTree.Enabled := not btnInitInsertTree.Enabled;;
end;

procedure TMainForm.actUpdatePopupControlsExecute(Sender: TObject);
begin
   actNodeAdd.Enabled := ClickedNode.Count < 2;
   actNodeDeleteChildren.Enabled := ClickedNode.Count > 0;

   actTreeInsertion.Enabled :=
     not((InsertTree = nil) or (WorkingTree = InsertTree) or
     (ClickedNode.Count = 2));
end;

{ >>> ACTIONS >>> MENU >>> }
procedure TMainForm.actExitExecute(Sender: TObject);
begin
   Close;
end;

procedure TMainForm.actHelpTaskExecute(Sender: TObject);
begin
   MessageDlg(TaskMsg, mtInformation, [mbOk], 0);
end;

procedure TMainForm.actHelpTipsExecute(Sender: TObject);
begin
   MessageDlg(TipsMsg, mtInformation, [mbOk], 0);
end;

{ >>> ACTIONS >>> MENU >>> }
{ >>> ACTIONS >>> }

{ >>> DRAWING TREES >>> }
procedure TMainForm.DrawChildren(TreeView: TTreeView; CurrentNode: TTreeNode;
  MyNode: PNode);
var
   NextNode: TTreeNode;
begin
   with TreeView.Items do
   begin
      if MyNode^.PRight <> nil then
      begin
         NextNode := AddChild(CurrentNode, MyNode.PRight^.Name);
         DrawChildren(TreeView, NextNode, MyNode.PRight);
      end;

      if MyNode^.PLeft <> nil then
      begin
         NextNode := AddChild(CurrentNode, MyNode.PLeft^.Name);
         DrawChildren(TreeView, NextNode, MyNode.PLeft);
      end;
   end;
end;

procedure TMainForm.DrawTrees(Sender: TObject);
var
   Head, Node1, Node2: TTreeNode;
   TreeNode: TTreeNode;
begin
   with TreeView.Items do
   begin
      Clear;
      if TreeTop <> nil then
      begin
         Head := Add(nil, TreeTop^.Name);
         DrawChildren(TreeView, Head, TreeTop);
      end;

   end;

   TreeView.FullExpand;

   with InsertTreeView.Items do
   begin
      Clear;
      if InsertTree <> nil then
      begin
         Head := Add(nil, InsertTree^.Name);
         DrawChildren(InsertTreeView, Head, InsertTree);
      end;

   end;
   actTreeUpdateControlsExecute(Sender);

   InsertTreeView.FullExpand;
end;
{ >>> DRAWING TREES >>> }

{ >>> POPUP >>> }
procedure TMainForm.mnPopPopup(Sender: TObject);
begin
   actUpdatePopupControlsExecute(Sender);
end;
{ >>> POPUP >>> }

{ >>> EVENTS >>> }
procedure TMainForm.TreeViewContextPopup(Sender: TObject; MousePos: TPoint;
  var Handled: Boolean);
var
   Child: TTreeNode;
   CurTree: TTreeView;
   CurGroupBox: TGroupBox;
begin
   CurTree := TTreeView(Sender);
   with CurTree.Items do
   begin
      ClickedNode := CurTree.GetNodeAt(MousePos.X, MousePos.Y);

      case CurTree.Tag of
         1:
            WorkingTree := TreeTop;
         2:
            WorkingTree := InsertTree;
      end;

      CurGroupBox := TGroupBox(CurTree.Parent);

      if ClickedNode <> nil then
         mnPop.Popup(MousePos.X + GetClientOrigin.X + CurTree.Left +
           CurGroupBox.Left, MousePos.Y + GetClientOrigin.Y + CurTree.Top +
           CurGroupBox.Top);
   end;
end;
{ >>> EVENTS >>> }

procedure TMainForm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
   CanClose := False;
   if MessageDlg(QuitMsg, mtInformation, mbOKCancel, 0) = mrOk then
      CanClose := True;
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
   WorkingTree := nil;
   TreeTop := nil;
   InsertTree := nil;
   FoundedNode := nil;
end;

function TMainForm.InputNodeName: string;
var
   Name: string;
begin
   Name := '';
   repeat
      Name := InputBox(EnterNodeNameMsg, NodeNameMsg, '');
      FindNode(WorkingTree, FoundedNode, Name);
   until (Name <> '') and (Length(Name) <= NameMaxLength) and
     not(NodeExists(TreeTop, FoundedNode, Name)) and
     not(NodeExists(InsertTree, FoundedNode, Name));

   Result := Name;
end;

end.
