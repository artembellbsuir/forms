object MainForm: TMainForm
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'Labwork #6.2'
  ClientHeight = 490
  ClientWidth = 767
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = mnMain
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  PixelsPerInch = 120
  TextHeight = 16
  object lblHistory: TLabel
    Left = 19
    Top = 447
    Width = 4
    Height = 16
  end
  object gbMainTree: TGroupBox
    Left = 19
    Top = 24
    Width = 350
    Height = 409
    Caption = 'Main Tree'
    TabOrder = 0
    object btnInitTree: TButton
      Left = 19
      Top = 33
      Width = 105
      Height = 25
      Action = actTreeInit
      TabOrder = 0
    end
    object btnClearTree: TButton
      Left = 219
      Top = 33
      Width = 113
      Height = 25
      Action = actTreeClear
      Enabled = False
      TabOrder = 1
    end
    object TreeView: TTreeView
      Tag = 1
      Left = 19
      Top = 72
      Width = 313
      Height = 273
      AutoExpand = True
      Indent = 19
      ReadOnly = True
      TabOrder = 2
      OnContextPopup = TreeViewContextPopup
    end
    object btnTraverse: TButton
      Left = 19
      Top = 365
      Width = 313
      Height = 25
      Action = actTreeLeftTopRightTraverse
      TabOrder = 3
    end
  end
  object gbInsertTree: TGroupBox
    Left = 383
    Top = 24
    Width = 354
    Height = 409
    Caption = 'Insert Tree'
    TabOrder = 1
    object InsertTreeView: TTreeView
      Tag = 2
      Left = 16
      Top = 72
      Width = 313
      Height = 273
      AutoExpand = True
      Indent = 19
      ReadOnly = True
      TabOrder = 0
      OnContextPopup = TreeViewContextPopup
    end
    object btnClearInsertTree: TButton
      Left = 216
      Top = 33
      Width = 113
      Height = 25
      Action = actTreeClearInsert
      Enabled = False
      TabOrder = 1
    end
    object btnInitInsertTree: TButton
      Left = 16
      Top = 33
      Width = 105
      Height = 25
      Action = actTreeInitInsert
      TabOrder = 2
    end
  end
  object mnPop: TPopupMenu
    OnPopup = mnPopPopup
    Left = 736
    Top = 40
    object mnPopAddNode: TMenuItem
      Action = actNodeAdd
    end
    object mnPopDelete: TMenuItem
      Action = actNodeDelete
    end
    object mnPopDeleteChildren: TMenuItem
      Action = actNodeDeleteChildren
    end
    object mnPopInsertTree: TMenuItem
      Action = actTreeInsertion
    end
  end
  object alMain: TActionList
    Left = 736
    Top = 104
    object actNodeAdd: TAction
      Category = 'Node'
      Caption = 'Add node'
      OnExecute = actNodeAddExecute
    end
    object actTreeInsertion: TAction
      Category = 'Tree'
      Caption = 'Insert tree'
      OnExecute = actTreeInsertionExecute
    end
    object actTreeClear: TAction
      Category = 'Tree'
      Caption = 'Clear Tree'
      OnExecute = actTreeClearExecute
    end
    object actNodeDelete: TAction
      Category = 'Node'
      Caption = 'Delete'
      OnExecute = actNodeDeleteExecute
    end
    object actTreeInitInsert: TAction
      Category = 'Tree'
      Caption = 'Init Insert Tree'
      OnExecute = actTreeInitInsertExecute
    end
    object actTreeClearInsert: TAction
      Category = 'Tree'
      Caption = 'Clear Insert Tree'
      OnExecute = actTreeClearInsertExecute
    end
    object actNodeDeleteChildren: TAction
      Category = 'Node'
      Caption = 'Delete children'
      OnExecute = actNodeDeleteChildrenExecute
    end
    object actTreeLeftTopRightTraverse: TAction
      Category = 'Tree'
      Caption = 'Traverse'
      OnExecute = actTreeLeftTopRightTraverseExecute
    end
    object actTreeInit: TAction
      Category = 'Tree'
      Caption = 'Init Tree'
      OnExecute = actTreeInitExecute
    end
    object actExit: TAction
      Category = 'File'
      Caption = 'actExit'
      OnExecute = actExitExecute
    end
    object actHelpTask: TAction
      Category = 'Help'
      Caption = 'Task'
      ShortCut = 16468
      OnExecute = actHelpTaskExecute
    end
    object actHelpTips: TAction
      Category = 'Help'
      Caption = 'Tips'
      ShortCut = 16457
      OnExecute = actHelpTipsExecute
    end
    object actTreeUpdateControls: TAction
      Category = 'Tree'
      Caption = 'actTreeUpdateControls'
      OnExecute = actTreeUpdateControlsExecute
    end
    object actUpdatePopupControls: TAction
      Category = 'Tree'
      Caption = 'actUpdatePopupControls'
      OnExecute = actUpdatePopupControlsExecute
    end
  end
  object mnMain: TMainMenu
    Left = 736
    Top = 160
    object mnItemHelp: TMenuItem
      Caption = 'Help'
      object mnItemTask: TMenuItem
        Action = actHelpTask
      end
      object mnItemTips: TMenuItem
        Action = actHelpTips
      end
    end
  end
end
